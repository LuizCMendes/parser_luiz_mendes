package com.ef.vo;

import java.math.BigInteger;
import java.util.Date;

public class AccessLogVO {
	
	private Long id;
	private Date dateAccess;	
	private String ip;
	private BigInteger counter;
	
	
	public AccessLogVO() {	}


	public Long getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id.longValue();
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateAccess() {
		return dateAccess;
	}

	public void setDateAccess(Date dateAccess) {
		this.dateAccess = dateAccess;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public BigInteger getCounter() {
		return counter;
	}

	public void setCounter(BigInteger counter) {
		this.counter = counter;
	}
	
}
