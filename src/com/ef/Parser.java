package com.ef;

import java.util.Date;

import com.ef.connection.Connection;
import com.ef.enums.DurationEnum;
import com.ef.service.AccessLogService;
import com.ef.service.BlockedIpService;
import com.ef.utils.DateUtils;
import com.ef.utils.StringUtils;

public class Parser {
	
	private String filename;
	private Date startDate;
	private DurationEnum duration;
	private Integer threshold;
	
	private AccessLogService accessLogService = new AccessLogService();
	private BlockedIpService blockedIpService = new BlockedIpService();
	

	public static void main(String[] args) {
		new Parser().run(args);
	}

	/**
	 * controls the log parser
	 * @param args
	 */
	private void run(String[] args) {
		try {
			Connection.start();
			
			verifyCorrectNumberOfParams(args);

			getVariablesValues(args);
					
			accessLogService.importLog(filename);
			
			blockedIpService.executeBlock(duration, startDate, threshold);
		
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			Connection.close();
			System.out.println("Program terminated");
		}
		
		System.exit(0);
	}

	/**
	 * checks if the minimum amount of params was passed
	 * @param args
	 * @throws Exception 
	 */
	private void verifyCorrectNumberOfParams(String[] args) throws Exception {
		if(args.length < 4) {
			throw new Exception ("Incorrect numbers parameters passed.");
		}
	}
	
	/**
	 * converts and fill in the values of the variables
	 * @param args
	 * @throws Exception
	 */
	private void getVariablesValues(String[] args) throws Exception {
		String startDateParameter = StringUtils.split(args[1], "=", 1);
		String periodParameter = StringUtils.split(args[2], "=", 1);
		String thresholdParameter = StringUtils.split(args[3], "=", 1);
		
		this.filename = StringUtils.split(args[0], "=", 1);
		this.startDate = DateUtils.getFormattedDate(startDateParameter,DateUtils.STANDARD_DATE_FORMAT);			
		this.duration = DurationEnum.getEnum(periodParameter);
		this.threshold = Integer.valueOf(thresholdParameter);
	}
}
