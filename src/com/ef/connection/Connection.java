package com.ef.connection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Connection{
	
	private static EntityManager em;

	public static EntityManager getEntityManager() {
		if(em == null) {
			start();
		}
		return em;
	}

	public static void beginTransaction() {
		em.getTransaction().begin();
	}

	public static void commitTransaction() {
		em.getTransaction().commit();
	}

	public static void start() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("parserPU");
		em = emf.createEntityManager();
	}

	public static void close() {
		em.close();
	}

}
