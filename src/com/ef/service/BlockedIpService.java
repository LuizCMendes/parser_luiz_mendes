package com.ef.service;

import java.util.Date;
import java.util.List;

import com.ef.connection.Connection;
import com.ef.dao.AccessLogDAO;
import com.ef.dao.BlockedIpDAO;
import com.ef.enums.DurationEnum;
import com.ef.model.BlockedIp;
import com.ef.utils.DateUtils;
import com.ef.vo.AccessLogVO;

public class BlockedIpService {
	
	private AccessLogDAO accessLogDAO = new AccessLogDAO();
	private BlockedIpDAO blockedIpDAO = new BlockedIpDAO();
	private final String BLOCK_MESSAGE = "Blocked due to excessive access";
	
	public void executeBlock(DurationEnum duration, Date startDate, Integer threshold) {
		Date endDate = null;
		if(DurationEnum.HOURLY.equals(duration)) {
			endDate = DateUtils.plusHours(startDate, 1L);
		}else {
			endDate = DateUtils.plusDays(startDate, 1L);
		}
		
		List<AccessLogVO> results = accessLogDAO.findRepetitiveAccess(startDate, endDate, threshold);
		
		if(results.size() > 0) {
			System.out.println("The following IP´s have been blocked due to excessive access");
		
			Connection.beginTransaction();
			for (AccessLogVO accessLogVO : results) {
				System.out.println("|" + accessLogVO.getIp() + "|");
				
				blockedIpDAO.save(new BlockedIp(new Date(), accessLogVO.getIp(), BLOCK_MESSAGE));
				
			}
			Connection.commitTransaction();
		}
	}
}
