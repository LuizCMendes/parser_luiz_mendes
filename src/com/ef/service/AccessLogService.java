package com.ef.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import com.ef.connection.Connection;
import com.ef.dao.AccessLogDAO;
import com.ef.model.AccessLog;
import com.ef.utils.StringUtils;

public class AccessLogService {

	private AccessLogDAO accessLogDAO = new AccessLogDAO();

	public void importLog(String filename) throws Exception {	
		System.out.println("Reading "+ filename);
		List<AccessLog> accessLogs = readLog(filename);
		System.out.println("Reading "+ filename + " has ended.");
		
		System.out.println("Importing access");
		Connection.beginTransaction();
		for (AccessLog accessLog : accessLogs) {
			accessLogDAO.save(accessLog);
		}
		Connection.commitTransaction();
		System.out.println("Imported all "+accessLogs.size()+" access");
	}

	
	private List<AccessLog> readLog(String filename) {
		List<AccessLog> result = new ArrayList<>(0);
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
				AccessLog accessLog = new AccessLog(StringUtils.split(sCurrentLine, "\\|", 0), StringUtils.split(sCurrentLine, "\\|", 1));
				result.add(accessLog);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
		
	}
}
