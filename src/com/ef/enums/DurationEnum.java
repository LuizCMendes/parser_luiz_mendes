package com.ef.enums;

public enum DurationEnum {
	HOURLY,
	DAILY;

	public static DurationEnum getEnum(String duration) throws Exception {
		duration = duration.toUpperCase().trim();
		
		switch (duration) {
		
		case "HOURLY":
			return DurationEnum.HOURLY;
			
		case "DAILY":
			return DurationEnum.DAILY;

		default:
			throw new Exception ("The duration informed is not valid. Valid values ​​are 'HOURLY' and 'DAILY'");
		}
	}

}
