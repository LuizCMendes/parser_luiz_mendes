package com.ef.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ef.utils.DateUtils;

@Entity
public class AccessLog {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateAccess;	
	
	private String ip;
	
	public AccessLog() {
		super();
	}

	public AccessLog(String stringAccessDate, String ip) throws Exception {
		this(DateUtils.getFormattedDate(stringAccessDate, DateUtils.MILLIS_DATE_FORMAT), ip);
	}

	public AccessLog(Date dateAccess, String ip) {
		this.dateAccess = dateAccess;
		this.ip = ip;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Date getDateAccess() {
		return dateAccess;
	}
	
	public void setDateAccess(Date dateAccess) {
		this.dateAccess = dateAccess;
	}
	
	public String getIp() {
		return ip;
	}
	
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	@Override
	public String toString() {
		return "Access log id: "+ id + ", accessed on: "+ dateAccess + ", from ip: "+ip;
	}
}
