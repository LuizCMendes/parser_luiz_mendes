package com.ef.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import com.ef.connection.Connection;
import com.ef.model.AccessLog;
import com.ef.vo.AccessLogVO;

public class AccessLogDAO {

	public AccessLog save(AccessLog accessLog) {
		accessLog = Connection.getEntityManager().merge(accessLog);
		return accessLog;
	}

	@SuppressWarnings("unchecked")
	public List<AccessLogVO> findRepetitiveAccess(Date startDate, Date endDate, Integer threshold) {
		List<AccessLogVO> returnList = new ArrayList<>(0);
		
		StringBuilder sb = new StringBuilder(0);
		sb.append(" SELECT id, dateAccess, ip, COUNT(*) ");
		sb.append(" FROM ");
		sb.append(" (");
		sb.append("		SELECT ip, dateAccess, id ");
		sb.append("		FROM accesslog ");
		sb.append("		WHERE dateAccess BETWEEN :startDate AND :endDate ");
		sb.append(" ) log ");
		sb.append(" GROUP BY ip");
		sb.append(" HAVING COUNT(*) > :threshold ");
		
		Query query = Connection.getEntityManager().createNativeQuery(sb.toString());
		
		query.setParameter("startDate", startDate);
		query.setParameter("endDate", endDate);
		query.setParameter("threshold", threshold);
		
		List<Object[]> result = query.getResultList();
		
		for (Object[] obj : result) {
			AccessLogVO vo = new AccessLogVO();
			vo.setId(((BigInteger) obj[0]));
			vo.setDateAccess( (Date) obj[1]);
			vo.setIp((String) obj[2]);
			vo.setCounter((BigInteger)obj[3]);
			
			returnList.add(vo);
		}
		
		return returnList;
	}
}
