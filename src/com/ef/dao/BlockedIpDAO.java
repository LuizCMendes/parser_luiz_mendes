package com.ef.dao;

import com.ef.connection.Connection;
import com.ef.model.BlockedIp;

public class BlockedIpDAO {
	
	public BlockedIp save(BlockedIp blockedIp) {
		blockedIp = Connection.getEntityManager().merge(blockedIp);
		return blockedIp;
	}

}
