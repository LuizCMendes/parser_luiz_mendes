package com.ef.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class DateUtils {
	
	public static final String STANDARD_DATE_FORMAT = "yyyy-MM-dd.HH:mm:ss";
	public static final String MILLIS_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";

	
	public static Date getFormattedDate(String dateAsString, String dateFormat) throws Exception {
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
		
		try {
			return formatter.parse(dateAsString);
		}catch (ParseException e) {
			throw new Exception("It was not possible to convert the date passed in parameter");
		}
	}

	/**
	 * adds to the @Date the hours @plusHours
	 * @param date
	 * @param plusHours
	 * @return
	 */
	public static Date plusHours(Date date, Long plusHours) {
		LocalDateTime ldt = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()).plusHours(plusHours);
		return getDate(ldt);
	}

	/**
	 * adds to the @Date the days @plusDays
	 * @param date
	 * @param plusDays
	 * @return
	 */
	public static Date plusDays(Date date, Long plusDays) {
		LocalDateTime ldt = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()).plusDays(plusDays);
		return getDate(ldt);
	}
	
	/**
	 * converts the @LocalDateTime to @Date
	 * @param ldt
	 * @return
	 */
	private static Date getDate(LocalDateTime ldt) {
		return Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
	}

}
