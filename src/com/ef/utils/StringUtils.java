package com.ef.utils;

public class StringUtils {

	/**
	 * Splits this @param str around matches of the given regular expression passed on @param splitter. 
	 * @param str is the string to split
	 * @param splitter is the regular expression used to split the string
	 * @param returnPart is the number (position) of the 'part' you want returned, zero (0) based
	 * @return string 
	 */
	public static String split (String str, String splitter, int returnPart) {
		String[] parts = str.split(splitter);
		return parts[returnPart];			
	}

}