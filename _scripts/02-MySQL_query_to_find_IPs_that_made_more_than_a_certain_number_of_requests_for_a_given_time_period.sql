/*
write mysql to find IPs that made more than a certain number of requests for a given time period.
*/


SET @startDateTime := '2017-01-01.13:00:00';
SET @endDateTime := '2017-01-01.14:00:00';
SET @threshold := 100;



SELECT id, dateAccess, ip, COUNT(*)
 FROM
    (SELECT ip, dateAccess, id FROM accesslog where dateAccess BETWEEN @startDateTime AND @endDateTime) log
 GROUP BY
    ip
HAVING COUNT(*) > @threshold;

